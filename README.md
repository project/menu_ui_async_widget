# CONTENTS OF THIS FILE

- Introduction
- Requirements
- Installation
- Configuration
- Maintainers


## INTRODUCTION

Inspired by [Metatag Asynchronous Widget](https://www.drupal.org/project/metatag_async_widget).
This module removes the menu selection UI from node forms and replaces it with
a button that loads the full UI if the user wants to add or edit the menu link
associated with the node.

In the scenario this module was developed for, two available menus with about
1500 links in each, the performance increase of loading the node form was
around 650%.

The impact of using this module will vary depending on your specific setup. If
you don't have large menus you will probably not see any performance gains.
Instead you will force the user to go through an extra step to add/edit the
menu link.

You might also want to take a look at these modules:

* [Metatag Asynchronous Widget](https://www.drupal.org/project/metatag_async_widget)
* [Trailless Menu](https://www.drupal.org/project/trailless_menu)
* [Big Menu](https://www.drupal.org/project/bigmenu)


## REQUIREMENTS

No special requirements besides the core module *Menu UI* enabled.


## INSTALLATION

Install as you would normally install a contributed Drupal module.
See: https://www.drupal.org/node/895232 for further information.


## CONFIGURATION

Configure the available menus at (/admin/structure/types/manage/<node_type>).

Click the tab *Menu UI Async settings* and select the available menus and the
default parent link.

Make sure you uncheck all menus under the tab *Menu settings*.


## MAINTAINERS

Current maintainers:
* Peter Törnstrand - https://www.drupal.org/u/peter-t%C3%B6rnstrand

This project has been sponsored by:
* Happiness - https://www.drupal.org/happiness
