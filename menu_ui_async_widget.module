<?php

use Drupal\Component\Utility\Html;
use Drupal\Core\Form\FormStateInterface;
use Drupal\system\MenuInterface;
use Drupal\menu_link_content\Entity\MenuLinkContent;
use Drupal\Core\Cache\CacheableMetadata;
use Drupal\node\NodeTypeInterface;
use Drupal\system\Entity\Menu;
use Drupal\node\NodeInterface;

/**
 * Returns the definition for a menu link for the given node.
 *
 * @param \Drupal\node\NodeInterface $node
 *   The node entity.
 *
 * @return array
 *   An array that contains default values for the menu link form.
 */
function menu_ui_async_widget_get_menu_link_defaults(NodeInterface $node) {
  // Prepare the definition for the edit form.
  /** @var \Drupal\node\NodeTypeInterface $node_type */
  $node_type = $node->type->entity;
  $menu_name = strtok($node_type->getThirdPartySetting('menu_ui_async_widget', 'parent', 'main:'), ':');
  $defaults = FALSE;
  if ($node->id()) {
    $id = FALSE;
    // Give priority to the default menu
    $type_menus = $node_type->getThirdPartySetting('menu_ui_async_widget', 'available_menus', ['main']);
    if (in_array($menu_name, $type_menus)) {
      $query = \Drupal::entityQuery('menu_link_content')
        ->accessCheck(TRUE)
        ->condition('link.uri', 'node/' . $node->id())
        ->condition('menu_name', $menu_name)
        ->sort('id', 'ASC')
        ->range(0, 1);
      $result = $query->execute();

      $id = (!empty($result)) ? reset($result) : FALSE;
    }
    // Check all allowed menus if a link does not exist in the default menu.
    if (!$id && !empty($type_menus)) {
      $query = \Drupal::entityQuery('menu_link_content')
        ->accessCheck(TRUE)
        ->condition('link.uri', 'entity:node/' . $node->id())
        ->condition('menu_name', array_values($type_menus), 'IN')
        ->sort('id', 'ASC')
        ->range(0, 1);
      $result = $query->execute();

      $id = (!empty($result)) ? reset($result) : FALSE;
    }
    if ($id) {
      $menu_link = MenuLinkContent::load($id);
      $menu_link = \Drupal::service('entity.repository')->getTranslationFromContext($menu_link);
      $defaults = [
        'entity_id' => $menu_link->id(),
        'id' => $menu_link->getPluginId(),
        'title' => $menu_link->getTitle(),
        'title_max_length' => $menu_link->getFieldDefinitions()['title']->getSetting('max_length'),
        'description' => $menu_link->getDescription(),
        'description_max_length' => $menu_link->getFieldDefinitions()['description']->getSetting('max_length'),
        'menu_name' => $menu_link->getMenuName(),
        'parent' => $menu_link->getParentId(),
        'weight' => $menu_link->getWeight(),
      ];
    }
  }

  if (!$defaults) {
    // Get the default max_length of a menu link title from the base field
    // definition.
    $field_definitions = \Drupal::service('entity_field.manager')->getBaseFieldDefinitions('menu_link_content');
    $max_length = $field_definitions['title']->getSetting('max_length');
    $description_max_length = $field_definitions['description']->getSetting('max_length');
    $defaults = [
      'entity_id' => 0,
      'id' => '',
      'title' => '',
      'title_max_length' => $max_length,
      'description' => '',
      'description_max_length' => $description_max_length,
      'menu_name' => $menu_name,
      'parent' => '',
      'weight' => 0,
    ];
  }
  return $defaults;
}

/**
 * Implements hook_form_BASE_FORM_ID_alter() for \Drupal\node\NodeForm.
 *
 * Adds menu item fields to the node form.
 *
 * @see menu_ui_form_node_form_submit()
 */
function menu_ui_async_widget_form_node_form_alter(&$form, FormStateInterface $form_state) {

  // @todo This must be handled in a #process handler.
  $node = $form_state->getFormObject()->getEntity();
  $defaults = menu_ui_async_widget_get_menu_link_defaults($node);

  $form['menu'] = [
    '#type' => 'details',
    '#title' => t('Menu settings'),
    '#access' => \Drupal::currentUser()->hasPermission('administer menu'),
    '#open' => (bool) $defaults['id'],
    '#group' => 'advanced',
    '#attached' => [
      'library' => ['menu_ui/drupal.menu_ui'],
    ],
    '#tree' => TRUE,
    '#weight' => -2,
    '#attributes' => ['class' => ['menu-link-form']],
  ];

  // Have user clicked to customize link?
  if ($form_state->get('menu_ui_async_widget')) {
    // Generate a list of possible parents (not including this link or descendants).
    /** @var \Drupal\node\NodeTypeInterface $node_type */
    $node_type = $node->type->entity;
    /** @var \Drupal\Core\Menu\MenuParentFormSelectorInterface $menu_parent_selector */
    $menu_parent_selector = \Drupal::service('menu.parent_form_selector');
    $type_menus_ids = $node_type->getThirdPartySetting('menu_ui_async_widget', 'available_menus', ['main']);
    if (empty($type_menus_ids)) {
      return;
    }
    /** @var \Drupal\system\MenuInterface[] $type_menus */
    $type_menus = Menu::loadMultiple($type_menus_ids);
    $available_menus = [];
    foreach ($type_menus as $menu) {
      $available_menus[$menu->id()] = $menu->label();
    }
    if ($defaults['id']) {
      $default = $defaults['menu_name'] . ':' . $defaults['parent'];
    }
    else {
      $default = $node_type->getThirdPartySetting('menu_ui_async_widget', 'parent', 'main:');
    }
    $parent_element = $menu_parent_selector->parentSelectElement($default, $defaults['id'], $available_menus);
    // If no possible parent menu items were found, there is nothing to display.
    if (empty($parent_element)) {
      return;
    }

    $form['menu']['container'] = [
      '#type' => 'container',
      '#parents' => ['menu'],
      '#tree' => TRUE,
    ];
    $form['menu']['container']['enabled'] = [
      '#type' => 'checkbox',
      '#title' => t('Provide a menu link'),
      '#default_value' => (int) (bool) $defaults['id'],
    ];

    $form['menu']['container']['link'] = [
      '#type' => 'container',
      '#parents' => ['menu'],
      '#states' => [
        'invisible' => [
          'input[name="menu[enabled]"]' => ['checked' => FALSE],
        ],
      ],
    ];

    // Populate the element with the link data.
    foreach (['id', 'entity_id'] as $key) {
      $form['menu']['container']['link'][$key] = ['#type' => 'value', '#value' => $defaults[$key]];
    }

    $form['menu']['container']['link']['title'] = [
      '#type' => 'textfield',
      '#title' => t('Menu link title'),
      '#default_value' => $defaults['title'],
      '#maxlength' => $defaults['title_max_length'],
    ];

    $form['menu']['container']['link']['description'] = [
      '#type' => 'textfield',
      '#title' => t('Description'),
      '#default_value' => $defaults['description'],
      '#description' => t('Shown when hovering over the menu link.'),
      '#maxlength' => $defaults['description_max_length'],
    ];

    $form['menu']['container']['link']['menu_parent'] = $parent_element;
    $form['menu']['container']['link']['menu_parent']['#title'] = t('Parent link');
    $form['menu']['container']['link']['menu_parent']['#attributes']['class'][] = 'menu-parent-select';

    $form['menu']['container']['link']['weight'] = [
      '#type' => 'number',
      '#title' => t('Weight'),
      '#default_value' => $defaults['weight'],
      '#description' => t('Menu links with lower weights are displayed before links with higher weights.'),
    ];
  }
  else {
    $wrapper_id = Html::getUniqueId('menu-ui-async-widget-wrapper');
    $form['menu']['customize'] = [
      '#type' => 'container',
    ];
    $form['menu']['customize']['submit'] = [
      '#type' => 'submit',
      '#name' => 'menu_ui_async_widget_add_edit_menu_link',
      '#value' => ((bool) $defaults['id']) ? t('Edit menu link') : t('Add menu link'),
      '#limit_validation_errors' => [],
      '#submit' => ['menu_ui_async_widget_node_form_submit'],
      '#ajax' => [
        'callback' => 'menu_ui_async_widget_ajax_callback',
        'wrapper' => $wrapper_id,
      ],
      '#prefix' => "<span id=\"$wrapper_id\">",
      '#suffix' => "</span>",
    ];
    if ($defaults['title']) {
      // We need these fields here to make the drupal.menu_ui JS lib display
      // the correct summary before we load the complete form.
      $form['menu']['customize']['enabled'] = [
        '#type' => 'checkbox',
        '#value' => (int) (bool) $defaults['id'],
        '#prefix' => '<span class="js-form-item-menu-enabled hidden">',
        '#suffix' => '</span>',
      ];
      $form['menu']['customize']['title'] = [
        '#type' => 'textfield',
        '#value' => $defaults['title'],
        '#prefix' => '<span class="js-form-item-menu-title hidden">',
        '#suffix' => '</span>',
      ];
    }
  }

  foreach (array_keys($form['actions']) as $action) {
    if ($action != 'preview' && isset($form['actions'][$action]['#type']) && $form['actions'][$action]['#type'] === 'submit') {
      $form['actions'][$action]['#submit'][] = 'menu_ui_form_node_form_submit';
    }
  }

  $form['#entity_builders'][] = 'menu_ui_node_builder';
}

/**
 * Form submission handler for add/edit menu item on the node form.
 */
function menu_ui_async_widget_node_form_submit(array $form, FormStateInterface $form_state) {
  $form_state->set('menu_ui_async_widget', TRUE);
  $form_state->setRebuild();
}

/**
 * AJAX form callback for loading additional form elements.
 */
function menu_ui_async_widget_ajax_callback(array $form, FormStateInterface $form_state) {
  $form_state->setRebuild();
  return $form['menu']['container'];
}

/**
 * Implements hook_form_FORM_ID_alter() for \Drupal\node\NodeTypeForm.
 *
 * Adds menu options to the node type form.
 *
 * @see NodeTypeForm::form()
 * @see menu_ui_form_node_type_form_builder()
 */
function menu_ui_async_widget_form_node_type_form_alter(&$form, FormStateInterface $form_state) {
  /** @var \Drupal\Core\Menu\MenuParentFormSelectorInterface $menu_parent_selector */
  $menu_parent_selector = \Drupal::service('menu.parent_form_selector');
  $menu_options = array_map(function (MenuInterface $menu) {
    return $menu->label();
  }, Menu::loadMultiple());
  asort($menu_options);

  /** @var \Drupal\node\NodeTypeInterface $type */
  $type = $form_state->getFormObject()->getEntity();

  $form['menu_async'] = [
    '#type' => 'details',
    '#title' => t('Menu UI Async settings'),
    '#attached' => [
      'library' => ['menu_ui_async_widget/menu_ui_async_widget.admin'],
    ],
    '#group' => 'additional_settings',
  ];

  // Inherit default menus values from default menu settings.
  $default_menus = $type->getThirdPartySetting('menu_ui', 'available_menus', ['main']);
  $default_menus_async = $type->getThirdPartySetting('menu_ui_async_widget', 'available_menus', $default_menus);

  $form['menu_async']['menu_async_options'] = [
    '#type' => 'checkboxes',
    '#title' => t('Available menus'),
    '#default_value' => $default_menus_async,
    '#options' => $menu_options,
    '#description' => t('The menus available to place links in for this content type.'),
  ];

  // @todo See if we can avoid pre-loading all options by changing the form or
  //   using a #process callback. https://www.drupal.org/node/2310319
  //   To avoid an 'illegal option' error after saving the form we have to load
  //   all available menu parents. Otherwise, it is not possible to dynamically
  //   add options to the list using ajax.
  $options_cacheability = new CacheableMetadata();
  $options = $menu_parent_selector->getParentSelectOptions('', NULL, $options_cacheability);

  // Inherit selected parent menu item from default menu settings.
  $default_menu_item = $type->getThirdPartySetting('menu_ui', 'parent', 'main:');
  $default_menu_item_async = $type->getThirdPartySetting('menu_ui_async_widget', 'parent', $default_menu_item);

  $form['menu_async']['menu_async_parent'] = [
    '#type' => 'select',
    '#title' => t('Default parent link'),
    '#default_value' => $default_menu_item_async,
    '#options' => $options,
    '#description' => t('Choose the menu link to be the default parent for a new link in the content authoring form.'),
    '#attributes' => ['class' => ['menu-title-select']],
  ];

  $options_cacheability->applyTo($form['menu_async']['menu_async_parent']);

  $form['#validate'][] = 'menu_ui_async_widget_form_node_type_form_validate';
  $form['#entity_builders'][] = 'menu_ui_async_widget_form_node_type_form_builder';
}

/**
 * Validate handler for forms with menu options.
 *
 * @see menu_ui_form_node_type_form_alter()
 */
function menu_ui_async_widget_form_node_type_form_validate(&$form, FormStateInterface $form_state) {
  $available_menus = array_filter($form_state->getValue('menu_async_options'));
  // If there is at least one menu allowed, the selected item should be in
  // one of them.
  if (count($available_menus)) {
    $menu_item_id_parts = explode(':', $form_state->getValue('menu_async_parent'));
    if (!in_array($menu_item_id_parts[0], $available_menus)) {
      $form_state->setErrorByName('menu_async_parent', t('The selected menu link is not under one of the selected menus.'));
    }
  }
  else {
    $form_state->setValue('menu_async_parent', '');
  }
}

/**
 * Entity builder for the node type form with menu options.
 *
 * @see menu_ui_form_node_type_form_alter()
 */
function menu_ui_async_widget_form_node_type_form_builder($entity_type, NodeTypeInterface $type, &$form, FormStateInterface $form_state) {
  $type->setThirdPartySetting('menu_ui_async_widget', 'available_menus', array_values(array_filter($form_state->getValue('menu_async_options'))));
  $type->setThirdPartySetting('menu_ui_async_widget', 'parent', $form_state->getValue('menu_async_parent'));
}
